package com.tanghd.thrift.netty.server.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

import com.tanghd.thrift.netty.server.codec.TNettyThriftByteToMessageDecoder;
import com.tanghd.thrift.netty.server.codec.TNettyThriftHandler;
import com.tanghd.thrift.netty.server.codec.TNettyThriftMessageToByteEncoder;

public class TNettyThriftInitializer extends ChannelInitializer<SocketChannel> {

    private TNettyThriftDef serverDef;

    public TNettyThriftInitializer(TNettyThriftDef serverDef) {
        this.serverDef = serverDef;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        //message && byte 
        pipeline.addLast(TNettyThriftByteToMessageDecoder.getName(), new TNettyThriftByteToMessageDecoder(serverDef));
        pipeline.addLast(TNettyThriftMessageToByteEncoder.getName(), new TNettyThriftMessageToByteEncoder());
        
        pipeline.addLast(TNettyThriftHandler.getName(), new TNettyThriftHandler(serverDef));

    }

}
