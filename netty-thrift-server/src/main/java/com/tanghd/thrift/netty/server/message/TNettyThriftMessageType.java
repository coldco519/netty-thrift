package com.tanghd.thrift.netty.server.message;

public enum TNettyThriftMessageType {
    FRAMED, UNFRAMED;
}
