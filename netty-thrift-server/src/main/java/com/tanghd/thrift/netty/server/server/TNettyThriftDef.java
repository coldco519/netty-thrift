package com.tanghd.thrift.netty.server.server;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServerEventHandler;
import org.apache.thrift.transport.TTransportFactory;

public class TNettyThriftDef {

    private TServerEventHandler eventHandler;

    private TProcessorFactory processorFactory;

    private TTransportFactory inputTransportFactory;
    private TTransportFactory outputTransportFactory;
    private TProtocolFactory inputProtocolFactory;
    private TProtocolFactory outputProtocolFactory;
    
    private int maxReadBufferLength;
    
    public int getMaxReadBufferLength() {
        return maxReadBufferLength;
    }
    public void setMaxReadBufferLength(int maxReadBufferLength) {
        this.maxReadBufferLength = maxReadBufferLength;
    }
    public TServerEventHandler getEventHandler() {
        return eventHandler;
    }
    public void setEventHandler(TServerEventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }
    public TProcessorFactory getProcessorFactory() {
        return processorFactory;
    }
    public void setProcessorFactory(TProcessorFactory processorFactory) {
        this.processorFactory = processorFactory;
    }
    public TTransportFactory getInputTransportFactory() {
        return inputTransportFactory;
    }
    public void setInputTransportFactory(TTransportFactory inputTransportFactory) {
        this.inputTransportFactory = inputTransportFactory;
    }
    public TTransportFactory getOutputTransportFactory() {
        return outputTransportFactory;
    }
    public void setOutputTransportFactory(TTransportFactory outputTransportFactory) {
        this.outputTransportFactory = outputTransportFactory;
    }
    public TProtocolFactory getInputProtocolFactory() {
        return inputProtocolFactory;
    }
    public void setInputProtocolFactory(TProtocolFactory inputProtocolFactory) {
        this.inputProtocolFactory = inputProtocolFactory;
    }
    public TProtocolFactory getOutputProtocolFactory() {
        return outputProtocolFactory;
    }
    public void setOutputProtocolFactory(TProtocolFactory outputProtocolFactory) {
        this.outputProtocolFactory = outputProtocolFactory;
    }
}
