package com.tanghd.thrift.netty.server.codec;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import com.tanghd.thrift.netty.server.message.TNettyThriftMessage;
import com.tanghd.thrift.netty.server.processor.TNettyThriftProcessor;
import com.tanghd.thrift.netty.server.server.TNettyThriftDef;
import com.tanghd.thrift.netty.server.transport.TNettyThriftTransport;

public class TNettyThriftHandler extends SimpleChannelInboundHandler<TNettyThriftMessage> {

    private static final String name = "MESSAGE_HANDLER";
    
    private TNettyThriftDef serverDef;

    public TNettyThriftHandler(TNettyThriftDef serverDef) {
        this.serverDef = serverDef;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TNettyThriftMessage msg) throws Exception {
        TNettyThriftTransport transport = new TNettyThriftTransport(msg);
        TNettyThriftProcessor processor = new TNettyThriftProcessor(serverDef, transport);
        processor.invoke();
        ctx.writeAndFlush(msg);
    }
    
    public static String getName(){
        return name;
    }

}